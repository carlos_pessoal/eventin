class AddDetailsToUsers < ActiveRecord::Migration[7.1]
  def up
    add_column :users, :cpf, :string
    add_column :users, :nome, :string
    add_column :users, :participante, :boolean, default: false
    add_column :users, :avaliador, :boolean, default: false
    add_column :users, :organizador, :boolean, default: false
  end

  def down
    remove_column :users, :cpf
    remove_column :users, :nome
    remove_column :users, :participante
    remove_column :users, :avaliador
    remove_column :users, :organizador
  end
end
