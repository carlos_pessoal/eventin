class CreateEventos < ActiveRecord::Migration[7.1]
  def change
    create_table :eventos do |t|
      t.string :titulo
      t.text :descricao
      t.string :local
      t.date :data_inicio
      t.date :data_fim

      t.timestamps
    end
  end
end
