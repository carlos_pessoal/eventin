class AddUserToEventos < ActiveRecord::Migration[7.1]
  def up
    add_reference :eventos, :user, foreign_key: true
  end

  def down
    remove_reference :eventos, :user
  end
end
