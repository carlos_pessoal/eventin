class Api::V1::EventosController < ApplicationController
  before_action :authenticate_request
  before_action :set_evento, only: %i[ show update destroy ]

  # GET /eventos
  def index
    @eventos = Evento.all

    render json: @eventos
  end

  # GET /eventos/1
  def show
    render json: @evento
  end

  # POST /eventos
  def create
    @evento = Evento.new(evento_params)

    if @evento.save
      render json: @evento, status: :created, location: api_v1_evento_url(@evento)
    else
      render json: @evento.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /eventos/1
  def update
    if @evento.update(evento_params)
      render json: @evento
    else
      render json: @evento.errors, status: :unprocessable_entity
    end
  end

  # DELETE /eventos/1
  def destroy
    @evento.destroy!
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    # tratativa de erro
    def set_evento
      @evento = Evento.find(params[:id])
      unless @evento
        render json: { error: "Evento not found" }, status: :not_found
      end
    end

    # Only allow a list of trusted parameters through.
    def evento_params
      params.require(:evento).permit(:titulo, :descricao, :local, :data_inicio, :data_fim)
    end
end
