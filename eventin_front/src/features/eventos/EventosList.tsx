import React, { useState, useEffect } from 'react';
import { API_URL } from "../../constants";

interface Evento {
  id: number;
  titulo: string;
  descricao: string;
}

function EventosList() {
  const [eventos, setEventos] = useState<Evento[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    async function loadEventos() {
      try {
        const response = await fetch(API_URL);
        if (response.ok) {
          const json = await response.json();
          setEventos(json);
        } else {
          throw new Error('Network response was not ok');
        }
      } catch (e) {
        setError("Erro ocorreu...");
        console.log("Ocorreu um erro", e);
      } finally {
        setLoading(false);
      }
    }
    loadEventos();
  }, []);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>{error}</div>;
  }

  return (
    <div>
      {eventos.map((evento: Evento) => (
        <div key={evento.id} className="evento-container">
          <h2>{evento.titulo}</h2>
          <p>{evento.descricao}</p>
        </div>
      ))}
    </div>
  );
}

export default EventosList;
