
import './App.css'
import EventosList from './features/eventos/EventosList'

function App() {
  return (
    <>
      <div className='app'>
        <EventosList/>
      </div>
    </>
  )
}

export default App
